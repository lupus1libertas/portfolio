'use strict';

$(function () {
    'use strict';

    $('.header .toggle-menu').on('click', function (event) {
        var menuId = $(event.target).data('toggleFor');
        var $menu = $(menuId);
        $menu.toggleClass('open');
    });
});