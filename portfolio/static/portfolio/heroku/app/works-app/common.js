"use strict";

$(function () {
    "use strict";

    $(".carousel__images").slick(config.slick);
});
'use strict';

(function () {
    'use strict';

    var global = window;
    global.config = {
        slick: {
            adaptiveHeight: true,
            autoplay: true,
            lazyLoad: 'progressive',
            // lazyLoad: 'ondemand',

            get prevArrow() {
                return $('.carousel__prev');
            },

            get nextArrow() {
                return $('.carousel__next');
            }
        },

        routes: {
            projectInfo: '/project-info/'
        }
    };
    // global.config.MOBILE_WIDTH = 550;
})();
'use strict';

(function () {
    'use strict';

    $(function () {
        // TODO: detail info preload block should be outside .project-info__body
        // or I need change Handlebars template for detail info
        var $modal = $('.project-info.modal');
        var modalTemplateSource = $('#modal-template').html();
        var modalTemplate = Handlebars.compile(modalTemplateSource);
        $modal.html(modalTemplate({}));

        $('.work-example').on('click', projectOnClickHandler);

        $modal.on('click', '.project-info__body', function (ev) {
            ev.stopPropagation();
        });

        $modal.on('click', '.project-info__close', projectModalClose);

        $modal.on('click', projectModalClose);
    });

    function projectModalClose(ev) {
        if (ev.target.title !== 'Close' && $(ev.target).closest('.project-info__body').length) {
            ev.stopPropagation();
            return;
        }
        var $modal = $('.project-info.modal');
        $modal.toggleClass('open');
        $modal.find('.carousel__images').slick('unslick');
        $modal.html('');
        $(document.body).css('overflow', '');
        ev.stopPropagation();
    }

    function projectOnClickHandler(event) {
        $(document.body).css('overflow', 'hidden');
        var modalTemplateSource = $('#modal-template').html();
        var modalTemplate = Handlebars.compile(modalTemplateSource);

        var $modal = $('.project-info.modal');
        $modal.html(modalTemplate({}));

        $modal.toggleClass('open');

        var $project = $(event.target).closest('.works-list__item');
        var projectId = $project.data('projectId');

        var ajaxParams = {};
        ajaxParams.url = config.routes.projectInfo + projectId;
        ajaxParams.success = projectLoadSuccess;
        $.ajax(ajaxParams);
    }

    function projectLoadSuccess(projectData) {
        var modalTemplateSource = $('#modal-template').html();
        var modalTemplate = Handlebars.compile(modalTemplateSource);

        var $modal = $('.project-info.modal');

        var modalContext = {
            header: projectData.title,
            description: projectData.description,
            responsibilities: projectData.my_responsibilities,
            technologies: projectData.technologies.join(', '),
            link: projectData.link,
            repository: projectData.repository,
            images: projectData.images
        };

        $modal.html(modalTemplate(modalContext));
        $modal.find('.project-info__preload').hide();

        var imagesContext = {
            images: []
        };

        projectData.images.forEach(function (image) {
            if (image.type === 'scr') {
                imagesContext.images.push({
                    link: image.link
                });
            }
        });

        if (imagesContext.images.length > 0) {
            var $carouselImages = $modal.find('.carousel__images');
            var imagesTemplateSource = $('#carousel-images-template').html();
            var imagesTemplate = Handlebars.compile(imagesTemplateSource);

            $carouselImages.html(imagesTemplate(imagesContext));
            $carouselImages.slick(config.slick);
            $carouselImages.on('lazyLoaded', slickLazyLoadedHandler);

            if (imagesContext.images.length === 1) {
                $modal.find('.carousel__panel').hide();
            }
        }
    }

    function slickLazyLoadedHandler(event, $slick, $image, imageSource) {
        $image.css('display', '');
        var _$container = $('.slick-track');
        var $images = _$container.children();
        if ($images.length === 1) {
            _$container.closest('.slick-list').css('height', $images[0].height);
        }
    }
})();
'use strict';

(function () {
  'use strict';

  Handlebars.registerHelper('showIfExists', function (data) {
    return data ? '' : 'hidden';
  });
})();
'use strict';

(function () {
    'use strict';

    var global = window;

    $(function () {
        $('.filter-toggle').on('click', filterToggleClickHandler);

        $('.filter-toggle').sticky({
            widthFromWrapper: false,
            wrapperClassName: 'filter-toggle-sticky-wrapper'
        });

        $('.works-filter').sticky({
            wrapperClassName: 'filter-sticky-wrapper'
        });
        $('.filter-sticky-wrapper').addClass('hidden-on-mobile');

        //call handler after time. If event triggered again, wait time again.
        // $(window).on('resize', $.debounce(50 ,windowResizeHandler));
        $(window).on('resize', windowResizeHandler);
        // var $whowWithRepositories = $('');

        var $showAllLink = $('#all-projects');
        $showAllLink.on('click', showAllClickHandler);

        var $checkboxes = $('.options__checkbox');
        $checkboxes.on('click', checkboxClickHandler);
    });

    function filterToggleClickHandler() {
        var $this = $(this);
        $this.toggleClass('active');
        $this.css('width', '');
        $('.filter-sticky-wrapper').toggleClass('hidden-on-mobile');
        $('.works-filter').sticky('update');
    }

    function showWithRepositoriesClickHandler() {}

    // function windowResizeHandler() {
    //     var $window = $(this);
    //     if($window.width() < global.MOBILE_WIDTH + 200 && $window.width() > global.MOBILE_WIDTH - 200){
    //       $('.filter-toggle').sticky('update');
    //     }
    // }

    function windowResizeHandler() {
        // const $window = $(this);
        $('.filter-toggle').sticky('update');
    }

    function showAllProjects() {
        var $checkboxes = $('.options__checkbox');
        $checkboxes.each(function (indx, checkbox) {
            checkbox.checked = false;
            $(checkbox).parent().removeClass('checked');
        });

        var $workProjects = $('.works-list__item');
        $workProjects.addClass('selected');
    }

    function showAllClickHandler() {
        showAllProjects();
    }

    function checkboxClickHandler(ev) {
        var $workProjects = $('.works-list__item');

        $workProjects.each(function (indx, project) {
            project.technologies = JSON.parse(project.dataset.technologies);
        });

        $(ev.target).parent().toggleClass('checked');
        var $selectedCheckboxes = $('.options__checkbox:checked');
        var selectedTechnologies = [];

        $selectedCheckboxes.each(function (indx, checkbox) {
            selectedTechnologies.push(checkbox.value);
        });

        $workProjects.each(function (indx, project) {
            // preserved the record for myself
            // var setProjectTechnologies = new Set(project.technologies);
            // var setSelectedTechnologies = new Set(selectedTechnologies);

            // intersect of 2 sets: result is those elements, which contains in the first and in the second sets
            // var result = [...setProjectTechnologies].filter(technolgy => setSelectedTechnologies.has(technolgy));
            // var result = project.technologies.filter(technolgy => setSelectedTechnologies.has(technolgy));
            var result = project.technologies.filter(function (technology) {
                return selectedTechnologies.includes(technology);
            });

            var matchResult = false;
            if (result.length === selectedTechnologies.length) {
                matchResult = true;
            }

            var $project = $(project);

            if (matchResult) {
                $project.addClass('selected');
            } else {
                $project.removeClass('selected');
            }
        });

        if ($selectedCheckboxes.length === 0) {
            showAllProjects();
        }
    }
})();