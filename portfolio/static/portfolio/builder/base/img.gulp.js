module.exports = {
  init: init
}

function init(gulp, plugins, config){
  gulp.task('img-copy-build', function(){
      return gulp.src(config.paths.in.img)
      .pipe(gulp.dest(config.paths.out.build.img));
  });

  gulp.task('img-copy-heroku', function(){
      return gulp.src(config.paths.in.img)
      .pipe(gulp.dest(config.paths.out.heroku.img));
  });

}
