module.exports = {
    init: init
}

function init(gulp, plugins, config) {
    // gulp.task('package-copy-dev', function(){
    //   return gulp.src(config.packageFiles.dev.in)
    //     .pipe(gulp.dest(config.packageFiles.dev.out))
    // });

    gulp.task('package-copy-build', makePackageCopy(gulp, plugins, config, 'build'));

    gulp.task('package-copy-heroku', makePackageCopy(gulp, plugins, config, 'heroku'));
}

function makePackageCopy(gulp, plugins, config, environment) {
    return function() {
        var generalIn = config.packageFiles[environment].in;
        var generalOut = config.packageFiles[environment].out;
        var inFiles = groupArray(generalIn);

        gulp.src(inFiles.simplePaths)
            .pipe(gulp.dest(generalOut));

        inFiles.specifiedPaths.forEach(function(element) {
            gulp.src(element[0])
                .pipe(gulp.dest(generalOut + '/' + element[1]))
        });
    }
}

function groupArray(mixedArray) {
    var specifiedPathsArray = [];
    var plainStringArray = [];

    mixedArray.forEach(function(element) {
        if (typeof element === 'string') {
            plainStringArray.push(element);
        } else if (typeof element === 'object') {
            specifiedPathsArray.push(element);
        } else {
            console.log(element, typeof element)
            throw new Error('Incorrect element type in packages array');
        }
    });

    return {
        simplePaths: plainStringArray,
        specifiedPaths: specifiedPathsArray
    };
}
