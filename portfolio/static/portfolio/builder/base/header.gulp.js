module.exports = {
  // jsConcatBuild: jsConcatBuild,
  init: init
}

var makeJsConcat = require('../jsconcat-template').makeJsConcat; 


function init(gulp, plugins, config){
  var appName = 'header';
  gulp.task('js-concat-header-build', makeJsConcat(gulp, plugins, config, appName, 'build'));
  gulp.task('js-concat-header-heroku', makeJsConcat(gulp, plugins, config, appName, 'heroku'));

}
