var makeJsConcat = require('../jsconcat-template').makeJsConcat;
var makeSass = require('../sass-template').makeSass;

function init(gulp, plugins, config) {
    var appName = 'worksApp';

    //js concat
    gulp.task('js-concat-works-app-build', makeJsConcat(gulp, plugins, config, appName, 'build'));
    gulp.task('js-concat-works-app-heroku', makeJsConcat(gulp, plugins, config, appName, 'heroku'));

    //sass build
    gulp.task('sass-works-app-build', makeSass(gulp, plugins, config, appName, 'build'));
    gulp.task('sass-works-app-heroku', makeSass(gulp, plugins, config, appName, 'heroku'));
}

module.exports = {
    init: init
}
