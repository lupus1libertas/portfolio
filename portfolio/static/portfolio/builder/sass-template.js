var EnvironmentFactoryMap = {
    heroku: makeSassHeroku,
    build: makeSassBuild
}

function makeSass(gulp, plugins, config, app, environment) {
    if (EnvironmentFactoryMap[environment] != undefined) {
        return EnvironmentFactoryMap[environment](gulp, plugins, config, app)
    } else {
        throw new Error('Undefined environment');
    }
}

function makeSassBuild(gulp, plugins, config, app) {
    return function() {
        return gulp.src(config.paths.in.scss[app])
            .pipe(plugins.sass().on('error', plugins.sass.logError))
            .pipe(gulp.dest(config.paths.out.build.css));
    }
}

function makeSassHeroku(gulp, plugins, config, app) {
    return function() {
        return gulp.src(config.paths.in.scss[app])
            .pipe(plugins.sass().on('error', plugins.sass.logError))
            .pipe(gulp.dest(config.paths.out.heroku.css));
    }
}

module.exports = {
    makeSass: makeSass
}
