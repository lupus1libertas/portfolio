var EnvironmentFactoryMap = {
    heroku: makeFunctionHeroku,
    build: makeFunctionBuid
}

function makeJsConcat(gulp, plugins, config, app, environment) {
    if (EnvironmentFactoryMap[environment] != undefined) {
        return EnvironmentFactoryMap[environment](gulp, plugins, config, app)
    } else {
        throw new Error('Undefined environment');
    }
}

/*
TODO: use this
function makeJsConcat(gulp, plugins, config, app, environment) {
    if (!EnvironmentFactoryMap[environment]) {
        throw new Error('Undefined environment');
    }
    return EnvironmentFactoryMap[environment](gulp, plugins, config, app)
}

*/

function makeFunctionBuid(gulp, plugins, config, app) {
    return function() {
        gulp.src(config.paths.in.js[app])
            .pipe(plugins.jshint(config.jshint))
            .pipe(plugins.jshint.reporter('default'))
            .pipe(plugins.babel(config.babel))
            .pipe(plugins.concat('common.js'))
            .pipe(gulp.dest(config.paths.out.build.js[app]));
    }
}

function makeFunctionHeroku(gulp, plugins, config, app) {
    return function() {
        gulp.src(config.paths.in.js[app])
            .pipe(plugins.jshint(config.jshint))
            .pipe(plugins.jshint.reporter('default'))
            .pipe(plugins.babel(config.babel))
            .pipe(plugins.concat('common.js'))
            .pipe(gulp.dest(config.paths.out.heroku.js[app]));
    }
}

module.exports = {
    makeJsConcat: makeJsConcat,
}
