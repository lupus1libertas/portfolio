'use strict'

// init block

var gulp = require('gulp')
var plugins = require('gulp-load-plugins')();
var config = require('./config');
var gulpTasks = {
    header: require('./base/header.gulp'),
    worksApp: require('./works-app/common.gulp'),
    package: require('./base/package.gulp'),
    img: require('./base/img.gulp')
};

Object.keys(gulpTasks).forEach(function(key) {
    gulpTasks[key].init(gulp, plugins, config);
});

//base tasks

gulp.task('js-concat-build', ['js-concat-header-build', 'js-concat-works-app-build']);
gulp.task('js-concat-heroku', ['js-concat-header-heroku', 'js-concat-works-app-heroku']);

gulp.task('sass-build', ['sass-works-app-build']);
gulp.task('sass-heroku', ['sass-works-app-heroku']);

//watchers

gulp.task('watch-packages', function() {
    gulp.watch(config.packageFiles.build.in, ['package-copy-build'])
});

gulp.task('watch-sass', function() {
    gulp.watch(config.paths.in.scss.common, ['sass-build']);
});

gulp.task('watch-js', function() {
    gulp.watch(config.paths.in.js.common, ['js-concat-build'])
});

gulp.task('watch-img', function() {
    gulp.watch(config.paths.in.img, ['img-copy-build'])
});

//make dist for environment

gulp.task('make-build', ['sass-build', 'img-copy-build', 'package-copy-build', 'js-concat-build']);

gulp.task('make-heroku', ['sass-heroku', 'img-copy-heroku', 'package-copy-heroku', 'js-concat-heroku']);

//run with this locally

gulp.task('default', ['watch-sass', 'watch-img', 'watch-js', 'watch-packages'])
