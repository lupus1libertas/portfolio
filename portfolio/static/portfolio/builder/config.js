var config = {};

config.paths = {
    bower: '',
    in: {
        scss: {
            common: ['../src/styles/**/*.scss'],
            worksApp: ['../src/styles/out/works-app.scss']
        },
        css: [],
        js: {
            common: ['../src/app/**/*.js'],
            header: ['../src/app/header/**/*.js'],
            worksApp: ['../src/app/works-app/**/*.js']
        },
        img: ['../src/images/**/*.{jpg,png,gif,svg}']
    },
    out: {
        build: {
            css: '../build/styles',
            js: {
                header: '../build/app/header',
                worksApp: '../build/app/works-app'
            },
            img: '../build/images'
        },
        heroku: {
            css: '../heroku/styles',
            js: {
                header: '../heroku/app/header',
                worksApp: '../heroku/app/works-app'
            },
            img: '../heroku/images'
        }
    }
};

config.packageFiles = {
    build: { in: [
            "../bower_components/jquery/dist/jquery.js",

            "../bower_components/slick-carousel/slick/slick.css",
            "../bower_components/slick-carousel/slick/slick-theme.css",
            "../bower_components/slick-carousel/slick/slick.js",

            "../bower_components/jquery-sticky/jquery.sticky.js",

            "../bower_components/jquery-throttle-debounce/jquery.ba-throttle-debounce.js",

            ["../bower_components/font-awesome/css/*", "font-awesome/css"],
            ["../bower_components/font-awesome/fonts/*", "font-awesome/fonts"],

            "../bower_components/handlebars/handlebars.js"


        ],
        out: '../build/packages'
    },
    heroku: { in: [
            "../bower_components/jquery/dist/jquery.js",

            "../bower_components/slick-carousel/slick/slick.css",
            "../bower_components/slick-carousel/slick/slick-theme.css",
            "../bower_components/slick-carousel/slick/slick.js",

            "../bower_components/jquery-sticky/jquery.sticky.js",

            "../bower_components/jquery-throttle-debounce/jquery.ba-throttle-debounce.js",

            ["../bower_components/font-awesome/css/*", "font-awesome/css"],
            ["../bower_components/font-awesome/fonts/*", "font-awesome/fonts"],

            "../bower_components/handlebars/handlebars.js"
        ],
        out: '../heroku/packages'
    }
};

config.babel = {
    presets: ['es2015']
};

config.jshint = {
    esversion: 6
};

module.exports = config;
