$(() => {
    'use strict';

    $('.header .toggle-menu').on('click', (event) => {
        const menuId = $(event.target).data('toggleFor');
        const $menu = $(menuId);
        $menu.toggleClass('open');
    });
});
