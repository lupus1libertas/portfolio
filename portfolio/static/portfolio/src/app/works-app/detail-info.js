(function() {
    'use strict';

    $(() => {
        // TODO: detail info preload block should be outside .project-info__body
        // or I need change Handlebars template for detail info
        const $modal = $('.project-info.modal');
        const modalTemplateSource = $('#modal-template').html();
        const modalTemplate = Handlebars.compile(modalTemplateSource);
        $modal.html(modalTemplate({}));

        $('.work-example').on('click', projectOnClickHandler);

        $modal.on('click', '.project-info__body', (ev) => {
            ev.stopPropagation();
        });

        $modal.on('click', '.project-info__close', projectModalClose);

        $modal.on('click', projectModalClose);

    });

    function projectModalClose(ev) {
        if (ev.target.title !== 'Close' && $(ev.target).closest('.project-info__body').length) {
            ev.stopPropagation();
            return;
        }
        const $modal = $('.project-info.modal');
        $modal.toggleClass('open');
        $modal.find('.carousel__images').slick('unslick');
        $modal.html('');
        $(document.body).css('overflow', '');
        ev.stopPropagation();
    }

    function projectOnClickHandler(event) {
        $(document.body).css('overflow', 'hidden');
        const modalTemplateSource = $('#modal-template').html();
        const modalTemplate = Handlebars.compile(modalTemplateSource);

        const $modal = $('.project-info.modal');
        $modal.html(modalTemplate({}));

        $modal.toggleClass('open');

        const $project = $(event.target).closest('.works-list__item');
        const projectId = $project.data('projectId');

        const ajaxParams = {};
        ajaxParams.url = config.routes.projectInfo + projectId;
        ajaxParams.success = projectLoadSuccess;
        $.ajax(ajaxParams);
    }

    function projectLoadSuccess(projectData) {
        const modalTemplateSource = $('#modal-template').html();
        const modalTemplate = Handlebars.compile(modalTemplateSource);

        const $modal = $('.project-info.modal');

        const modalContext = {
            header: projectData.title,
            description: projectData.description,
            responsibilities: projectData.my_responsibilities,
            technologies: projectData.technologies.join(', '),
            link: projectData.link,
            repository: projectData.repository,
            images: projectData.images
        };

        $modal.html(modalTemplate(modalContext));
        $modal.find('.project-info__preload').hide();

        const imagesContext = {
            images: [],
        };

        projectData.images.forEach((image) => {
            if (image.type === 'scr') {
                imagesContext.images.push({
                    link: image.link,
                });
            }
        });

        if (imagesContext.images.length > 0) {
            const $carouselImages = $modal.find('.carousel__images');
            const imagesTemplateSource = $('#carousel-images-template').html();
            const imagesTemplate = Handlebars.compile(imagesTemplateSource);

            $carouselImages.html(imagesTemplate(imagesContext));
            $carouselImages.slick(config.slick);
            $carouselImages.on('lazyLoaded', slickLazyLoadedHandler);

            if (imagesContext.images.length === 1) {
                $modal.find('.carousel__panel').hide();
            }
        }
    }

    function slickLazyLoadedHandler(event, $slick, $image, imageSource) {
        $image.css('display', '');
        const _$container = $('.slick-track');
        const $images = _$container.children();
        if ($images.length === 1) {
            _$container.closest('.slick-list').css('height', $images[0].height);
        }
    }
})();
