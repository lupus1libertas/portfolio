(function () {
    'use strict';

    const global = window;
    global.config = {
        slick: {
            adaptiveHeight: true,
            autoplay: true,
            lazyLoad: 'progressive',
            // lazyLoad: 'ondemand',

            get prevArrow(){
              return $('.carousel__prev');
            },

            get nextArrow(){
              return $('.carousel__next');
            }
        },

        routes: {
            projectInfo: '/project-info/',
        }
    };
    // global.config.MOBILE_WIDTH = 550;
})();
