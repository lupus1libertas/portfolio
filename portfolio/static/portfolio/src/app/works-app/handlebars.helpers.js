(function(){
  'use strict';
  
  Handlebars.registerHelper('showIfExists', data => data ? '' : 'hidden');
})();
