(function() {
    'use strict';

    const global = window;

    $(() => {
        $('.filter-toggle').on('click', filterToggleClickHandler);

        $('.filter-toggle').sticky({
            widthFromWrapper: false,
            wrapperClassName: 'filter-toggle-sticky-wrapper',
            // responsiveWidth: true
        });

        $('.works-filter').sticky({
            wrapperClassName: 'filter-sticky-wrapper',
            // responsiveWidth: true
        });
        $('.filter-sticky-wrapper').addClass('hidden-on-mobile');

        //call handler after time. If event triggered again, wait time again.
        // $(window).on('resize', $.debounce(50 ,windowResizeHandler));
        $(window).on('resize', windowResizeHandler);
        // var $whowWithRepositories = $('');

        const $showAllLink = $('#all-projects');
        $showAllLink.on('click', showAllClickHandler);

        const $checkboxes = $('.options__checkbox');
        $checkboxes.on('click', checkboxClickHandler);
    });


    function filterToggleClickHandler() {
        const $this = $(this);
        $this.toggleClass('active');
        $this.css('width', '');
        $('.filter-sticky-wrapper').toggleClass('hidden-on-mobile');
        $('.works-filter').sticky('update');
    }

    function showWithRepositoriesClickHandler() {

    }

    // function windowResizeHandler() {
    //     var $window = $(this);
    //     if($window.width() < global.MOBILE_WIDTH + 200 && $window.width() > global.MOBILE_WIDTH - 200){
    //       $('.filter-toggle').sticky('update');
    //     }
    // }

    function windowResizeHandler() {
        // const $window = $(this);
        $('.filter-toggle').sticky('update');
    }

    function showAllProjects() {
        const $checkboxes = $('.options__checkbox');
        $checkboxes.each((indx, checkbox) => {
            checkbox.checked = false;
            $(checkbox).parent().removeClass('checked');
        });

        const $workProjects = $('.works-list__item');
        $workProjects.addClass('selected');
    }

    function showAllClickHandler() {
        showAllProjects();
    }

    function checkboxClickHandler(ev) {
        const $workProjects = $('.works-list__item');

        $workProjects.each((indx, project) => {
            project.technologies = JSON.parse(project.dataset.technologies);
        });

        $(ev.target).parent().toggleClass('checked');
        const $selectedCheckboxes = $('.options__checkbox:checked');
        const selectedTechnologies = [];

        $selectedCheckboxes.each((indx, checkbox) => {
            selectedTechnologies.push(checkbox.value);
        });

        $workProjects.each((indx, project) => {
            // preserved the record for myself
            // var setProjectTechnologies = new Set(project.technologies);
            // var setSelectedTechnologies = new Set(selectedTechnologies);

            // intersect of 2 sets: result is those elements, which contains in the first and in the second sets
            // var result = [...setProjectTechnologies].filter(technolgy => setSelectedTechnologies.has(technolgy));
            // var result = project.technologies.filter(technolgy => setSelectedTechnologies.has(technolgy));
            const result = project.technologies.filter(technology => selectedTechnologies.includes(technology));

            let matchResult = false;
            if (result.length === selectedTechnologies.length) {
                matchResult = true;
            }

            var $project = $(project);

            if (matchResult) {
                $project.addClass('selected');
            } else {
                $project.removeClass('selected');
            }
        });

        if ($selectedCheckboxes.length === 0) {
            showAllProjects();
        }

    }

})();
