(function() {
    angular
        .module('app')
        .config(routesSetter);

    function routesSetter($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/goods/");
        $stateProvider
            .state('goods', {
                url: "/goods/",
                templateUrl: "/static/goods/javascript/app/goods/templates/goods-list.html",
                controller: "goodsController as goods",
                // resolve: {
                //     goodsPreloaded: goodsPreloaded
                // }
            })
            .state('goods.item', {
                url: "{itemId}",
                templateUrl: '/static/goods/javascript/app/goods/templates/item-description.html',
                controller: "itemDescriptionController as description",
            });
    }

})();
