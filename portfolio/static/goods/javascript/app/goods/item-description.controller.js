(function() {
    angular.module('app').controller('itemDescriptionController', itemDescriptionController);

    itemDescriptionController.$inject = ['goodsService', 'appConfig', '$stateParams'];

    function itemDescriptionController(goodsService, appConfig, $stateParams) {

      var self_scope = this;
      var itemId = parseInt($stateParams.itemId);
      var productReviews = [];
      goodsService.getProductReviews(itemId).then(function(responce){
          self_scope.productReviews = responce.data;
          console.log(responce);
      });
      self_scope.item = goodsService.getProductObject(itemId);
      self_scope.hideDescription = false;
    }
})();
