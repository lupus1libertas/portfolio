(function() {
    angular.module('app').controller('goodsController', goodsController);

    goodsController.$inject = [ /*'goodsPreloaded', */ 'goodsService', 'userService', 'appConfig', '$http', '$timeout', '$rootScope', '$scope'];

    function goodsController( /*goodsPreloaded,*/ goodsService, userService, appConfig, $http, $timeout, $rootScope, $scope) {
        var self_scope = this;
        self_scope.items = []; //goodsPreloaded.items;

        self_scope.userLogined = userService.userLogined();

        goodsService.getProducts().then(function(responce) {
            self_scope.items = responce.data;
            self_scope.items.forEach(function(item) {
                loadReviews(item);
            });
        });

        $rootScope.$on('userLogined', onUserLogined);
        $rootScope.$on('userLogout', onUserLogout);

        self_scope.toggleReviews = toggleReviews;
        self_scope.sendReview = sendReview;


        function onUserLogined(event, data) {
            self_scope.userLogined = userService.userLogined();
        }

        function onUserLogout(event, data) {
            self_scope.userLogined = userService.userLogined();
        }

        function toggleReviews(item) {
            if (item.isCollapsed) {
                loadReviews(item);
                $timeout(function() {
                    $scope.$broadcast('rzSliderForceRender');
                });
            }
            item.isCollapsed = !item.isCollapsed
        }

        function loadReviews(item) {
            self_scope.userLogined = userService.userLogined();
            item.reviews = [];
            goodsService.getProductReviews(item.id).then(function(responce) {
                item.reviews = responce.data;
            });
        }

        function sendReview(item) {
            console.log($http)
            $http({
                url: appConfig.api_root_url + "/api/reviews/" + item.id,
                method: "POST",
                headers: {
                    'Authorization': 'Token ' + userService.getToken(),
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    rate: item.userRate.value,
                    text: item.userReview
                }
            }).then(function(response) {
                if (response.data.success) {
                  loadReviews(item);
                  item.userRate.value = goodsService.maxItemRate;
                  item.userReview = "";
                } else {
                    alert(response.data.message);
                }
            }, function(response) {
                alert('error when send review');
            })

            function addReviewOnSite() {

            }
        }
    }
})();
