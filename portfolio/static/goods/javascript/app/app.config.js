(function(){
  angular.module('app')
  .constant('appConfig', new AppConfig());

  function AppConfig(){
    this.api_root_url = 'https://cryptic-coast-60679.herokuapp.com';
    this.api_static = this.api_root_url + '/static/';
  }
})();
