(function() {
    angular.module('app').controller('menuController', menuController);

    menuController.$inject = ['appConfig', 'userService', '$uibModal' ,'$rootScope'];

    function menuController(appConfig, userService, $uibModal, $rootScope) {
        var self_scope = this;
        initData();

        self_scope.login = login;
        self_scope.register = register;
        self_scope.logout = logout;

        function initData(){
          self_scope.userLogined = userService.userLogined();
          if(self_scope.userLogined){
            self_scope.username = userService.getUsername();
            $rootScope.$emit('userLogined');
          }else{
            self_scope.username = '';
          }
        }

        function userEnterSuccess(response) {
            if (response.data.success) {
                self_scope.userLogined = userService.userLogined();
                $rootScope.$emit('userLogined');
            } else {
                alert(response.data.message);
            }
            return response;
        }

        function userEnterFail(responce) {
            alert(responce);
        }

        function login() {
            var loginInstance = $uibModal.open({
                animation: true,
                templateUrl: "/static/goods/javascript/app/user-management/templates/login-form.html",
                controller: 'loginFormController as loginForm',
                size: 'lg'
            });

            loginInstance.result.then(function(result) {
                userService.login(result.login, result.password).then(userEnterSuccess, userEnterFail);
            });
        }

        function register() {
            console.log('register');

            var registerInstance = $uibModal.open({
                animation: true,
                templateUrl: "/static/goods/javascript/app/user-management/templates/register-form.html",
                controller: 'registerFormController as registerForm',
                size: 'lg'
            });

            registerInstance.result.then(function(result) {
                userService.register(result.login, result.password).then(userEnterSuccess, function(response) {
                    console.log('error when register');
                    return response;
                });
            });

        }

        function logout() {
          userService.logout();
          initData();
          $rootScope.$emit('userLogout');
        }
    }
})();
