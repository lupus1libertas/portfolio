(function() {
    angular.module('app').controller('registerFormController', registerFormController);

    registerFormController.$inject = ['$uibModalInstance'];

    function registerFormController($uibModalInstance) {
        var self_scope = this;
        self_scope.passwordFieldType = 'password';
        self_scope.loginStr = '';
        self_scope.passwordStr = '';

        self_scope.register = register;
        self_scope.cancel = cancel;
        self_scope.showPassword = showPassword;

        function register(){
          console.log('register')
          var result = {
            login: self_scope.loginStr,
            password: self_scope.passwordStr
          }
          $uibModalInstance.close(result)
        }

        function cancel(){
          console.log('cancel')
          var reason = {
            msg: 'click on cancel'
          }
          $uibModalInstance.dismiss(reason)
        }

        function showPassword() {
            self_scope.passwordFieldType = self_scope.passwordFieldType == 'password' ? 'text' : 'password';
        }
    }
})();
