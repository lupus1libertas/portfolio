(function() {
    angular.module('app').controller('loginFormController', loginFormController);

    loginFormController.$inject = ['$uibModalInstance'];

    function loginFormController($uibModalInstance) {
        var self_scope = this;
        self_scope.passwordFieldType = 'password';
        self_scope.loginStr = '';
        self_scope.passwordStr = '';

        self_scope.login = login;
        self_scope.cancel = cancel;
        self_scope.showPassword = showPassword;

        function login(){
          console.log('login')
          var result = {
            login: self_scope.loginStr,
            password: self_scope.passwordStr
          }
          $uibModalInstance.close(result)
        }

        function cancel(){
          console.log('cancel')
          var reason = {
            msg: 'click on cancel'
          }
          $uibModalInstance.dismiss(reason)
        }

        function showPassword() {
            self_scope.passwordFieldType = self_scope.passwordFieldType == 'password' ? 'text' : 'password';
        }
    }
})();
