(function() {
    angular.module('app').service('goodsService', goodsService);

    function goodsService($http, appConfig) {
        this.getProducts = getProducts;
        this.getProductObject = getProductObject;
        this.getProductReviews = getProductReviews;
        this.minItemRate = 0;
        this.maxItemRate = 5;
        var self = this;
        var currentListProducts = {};

        function getProducts(options) {
            return $http({
                url: appConfig.api_root_url + "/api/products/",
                method: "GET",
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                params: options
            }).then(function(responce) {
                responce.data.forEach(function(elem) {
                    console.log(elem);
                    elem.isCollapsed = true;
                    elem.img = appConfig.api_static + elem.img;
                    var userRate = {
                        value: self.maxItemRate // range of user evaluations for each item is [0,5]
                    }
                    elem.userRate = userRate;
                    currentListProducts[elem.id] = elem;
                });
                // console.log(currentListProducts);
                return responce;
            });
        }

        function getProductObject(id) {
            return currentListProducts[id];
        }

        function getProductReviews(id) {
            return $http({
                url: appConfig.api_root_url + "/api/reviews/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
            }).then(function(responce) {
                responce.data.forEach(function(elem) {
                    elem.created_at = new Date(elem.created_at)
                });
                return responce;
            })
        }
    };
})();
