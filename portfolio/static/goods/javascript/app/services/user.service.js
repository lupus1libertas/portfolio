(function() {
    angular.module('app').service('userService', userService);

    function userService($http, $sessionStorage, appConfig) {
        var self = this;
        initData.call(this);

        this.userLogined = userLogined;
        this.register = register;
        this.login = login;
        this.getToken = getToken;
        this.getUsername = getUsername;
        this.logout = logout;

        function clearSessionStorage(){
          setSessionStorage(undefined, undefined);
        }

        function setSessionStorage(username, token){
          $sessionStorage.username = username;
          $sessionStorage.token = token;
        }

        function initData(){
          this._username = $sessionStorage.username || undefined;
          this._userLoginedFlag = $sessionStorage.token ? true : false;
          this._token = $sessionStorage.token || undefined;
        }

        function getUsername() {
          return this._username;
        }

        function getToken(){
          return this._token;
        }

        function userLogined() {
            return this._userLoginedFlag;
        }

        function register(username, password) {
            return $http({
                url: appConfig.api_root_url + "/api/register/",
                method: "POST",
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    username: username,
                    password: password
                }
            }).then(function(response) {
                    self._userLoginedFlag = true;
                    self._token = response.data.token;
                    setSessionStorage(response.config.data.username, response.data.token);
                    return response;
                },
                function(response) {
                    console.log('register error', response);
                    return response;
                });
        }

        function login(username, password) {
            return $http({
                url: appConfig.api_root_url + "/api/login/",
                method: "POST",
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                data: {
                    username: username,
                    password: password
                }
            }).then(function(response) {
                    self._userLoginedFlag = true;
                    self._token = response.data.token;
                    setSessionStorage(response.config.data.username, response.data.token);
                    return response;
                },
                function(response) {
                    console.log('login error', response);
                    clearSessionStorage();
                    return response;
                });
        }

        function logout() {
          clearSessionStorage();
          initData.call(this);//or use self in funtion initData
        }
    }
})();
