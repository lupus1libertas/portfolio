(function() {
    angular.module('app')
        .filter('slashedDate', function() {
            return function(date) {
                return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
            }
        })
})();
