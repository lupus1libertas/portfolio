import os

def environment(request):
    return {'USED_SERVER': os.environ.get('USED_SERVER', None)}
