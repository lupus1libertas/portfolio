# Parse database configuration from $DATABASE_URL
import dj_database_url
from settings import DATABASES
DATABASES['default'] =  dj_database_url.config()
