from __future__ import unicode_literals

from django.apps import AppConfig


class GoodsAppConfig(AppConfig):
    name = 'goods_app'
