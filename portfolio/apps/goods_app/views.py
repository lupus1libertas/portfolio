from django.views.generic import TemplateView

class GoodsView(TemplateView):
    template_name = "goods_app/index.html"
