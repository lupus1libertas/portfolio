from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import views

urlpatterns = [
    url(r'^$', views.GoodsView.as_view(), name='goods'),
]
