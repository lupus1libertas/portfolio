from __future__ import unicode_literals

from django.apps import AppConfig


class YourServiceAppConfig(AppConfig):
    name = 'your_service_app'
