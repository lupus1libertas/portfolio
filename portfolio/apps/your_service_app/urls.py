from django.conf.urls import url, include
from django.contrib import admin
import views

urlpatterns = [
    url(r'', views.YourServiceView.as_view(), name='you_service')
]
