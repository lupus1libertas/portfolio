from django.views.generic import TemplateView

class YourServiceView(TemplateView):
    template_name = "your_service_app/index.html"
