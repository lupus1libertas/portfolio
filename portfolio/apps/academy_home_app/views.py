from django.views.generic import TemplateView


class AcademyHomeView(TemplateView):
    template_name = "academy_home_app/index.html"


class AcademyAboutView(TemplateView):
    template_name = "academy_home_app/about_us.html"
