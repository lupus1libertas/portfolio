from django.conf.urls import url, include
from django.contrib import admin
import views

urlpatterns = [
    url(r'^$', views.AcademyHomeView.as_view(), name='academy_home'),
    url(r'^about_us/', views.AcademyAboutView.as_view(), name='academy_about')
]
