from __future__ import unicode_literals

from django.apps import AppConfig


class AcademyHomeAppConfig(AppConfig):
    name = 'academy_home_app'
