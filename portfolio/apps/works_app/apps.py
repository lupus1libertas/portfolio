from __future__ import unicode_literals

from django.apps import AppConfig


class WorksAppConfig(AppConfig):
    name = 'works_app'
