from django.conf.urls import url, include
import views


urlpatterns = [
    url(r'^$', views.main, name="main"),
    url(r'^project-info/(?P<pk>\d+)', views.project_info, name="project_info")
]
