from django.contrib import admin
from django import forms
import models
# Register your models here.


admin.site.register(models.Project)
admin.site.register(models.Technology)
admin.site.register(models.Repository)
admin.site.register(models.Image)
admin.site.register(models.Customer)
admin.site.register(models.CustomerLink)
admin.site.register(models.Review)
