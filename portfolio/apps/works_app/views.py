from django.shortcuts import render
from django.http import JsonResponse
from django.db.models import Model
from models import Project

# Create your views here.

def main(request):
    context = {}
    projects_queryset = Project.objects.filter(hidden=False)
    projects = [project.to_dict() for project in projects_queryset]

    context['projects'] = projects
    template_name = 'works_app/main.html'
    return render(request, template_name, context)

def project_info(request, pk):
    try:
        project_dict = Project.objects.get(pk=pk).to_dict()
    except Model.DoesNotExist:
        project_dict = {'errorMsg': 'object not found', 'error': True}
    return JsonResponse(project_dict)
