from django.core import validators

# when save url without domain, will use this validators
# it replaces original url string without domain to string with fake domain
class ResourseURLValidator(validators.URLValidator):
    def __call__(self, value):
        # value = force_text(value)
        if len(value) > 0:
            if value[0] == '/':
                value = 'http://www.fakedomain.com' + value
        # raise Exception(value)
        super(ResourseURLValidator, self).__call__(value)
