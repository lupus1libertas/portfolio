from __future__ import unicode_literals
from django.conf import settings
from django.db import models
import validators
import form_fields
import json

# Create your models here.

class Project(models.Model):
    hidden = models.BooleanField(default=False)
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    my_responsibilities = models.TextField(blank=True)
    language = models.CharField(max_length=20, blank=True)
    repository = models.ForeignKey('Repository', null=True, blank=True, related_name="projects")
    link = models.CharField(blank=True, max_length=2048)
    technologies = models.ManyToManyField('Technology', null=True, blank=True, related_name="projects")

    def __str__(self):
        return self.title

    def to_dict(self):
        project_dict = {}
        project_dict['pk'] = self.pk
        project_dict['title'] = self.title
        project_dict['description'] = self.description
        project_dict['my_responsibilities'] = self.my_responsibilities
        project_dict['language'] = self.language
        project_dict['hidden'] = self.hidden
        project_dict['technologies'] = [technolgy.name for technolgy in self.technologies.all()]
        technologies_lowercase = map(lambda tech: tech.lower(), project_dict['technologies'])
        project_dict['technology_values_json'] = json.dumps(technologies_lowercase)
        images = [{'type': image.type, 'link': image.link} for image in self.images.all()]
        project_dict['images'] = images
        thumnails = [image for image in images if image['type'] == 'thmb']
        project_dict['thumbnail'] = thumnails[0] if len(thumnails) > 0 else None
        project_dict['link'] = self.link
        project_dict['repository'] = self.repository.link if self.repository else None

        return project_dict


class Technology(models.Model):
    name = models.CharField(max_length=50,blank=True)

    def __str__(self):
        return self.name


class Repository(models.Model):
    link = models.URLField(blank=True)

    def __str__(self):
        return self.link


class ResourseURLField(models.URLField):
    default_validators = [validators.ResourseURLValidator()]

    def formfield(self, **kwargs):
        # As with CharField, this will cause URL validation to be performed
        # twice.
        defaults = {
            'form_class': form_fields.ResourseURLField,
        }
        defaults.update(kwargs)
        # raise Exception(self.__dict__)
        return super(models.URLField, self).formfield(**defaults)



class ResourseManager(models.Manager):
    def get_queryset(self):
        queryset = super(ResourseManager,self).get_queryset()
        if hasattr(settings, 'RESOURCE_MODE'):
            return queryset.filter(resource_mode=settings.RESOURCE_MODE)
        else:
            return queryset


class Resource(models.Model):
    resource_mode = models.CharField(max_length=20, blank=True) #TODO redo resource_mode as list of variants
    link = ResourseURLField()
    objects = ResourseManager()

    class Meta:
        abstract = True


class Image(Resource):
    IMAGE_TYPE_CHOICES = (
        ('thmb', 'thumbnail'),
        ('scr', 'screen')
    )
    project = models.ForeignKey('Project', on_delete=models.SET_NULL, null=True, blank=True, related_name='images')
    type = models.CharField(max_length=20, choices=IMAGE_TYPE_CHOICES)


class Customer(models.Model):
    name = models.CharField(max_length=100)


class CustomerLink(models.Model):
    customer = models.ForeignKey('Customer', on_delete=models.SET_NULL, null=True, related_name='links')
    link = models.URLField()


class Review(models.Model):
    project = models.ForeignKey('Project', on_delete=models.SET_NULL, null=True, related_name="revews")
    customer = models.ForeignKey('Customer', null=True, related_name="reviews")
    reviewCustomer = models.TextField(blank=True)
    rating = models.IntegerField(blank=True)
    datetime = models.DateTimeField(blank=True)
