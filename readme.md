For running this project make sure you have installed python2, virtualenv, pip, nodejs, bower, npm.
Requirements are in requirements.txt for python packages and requirements for front end are in portfolio/static/portfolio folder. Use bower and npm for installing front end dependencies.

To install and run server on windows or other OS read instructions for virtualenv and use alternative python package managers for your operation system.

Using virtualenv for installing python packages is recommended.

portfolio projects static source files placed in portfolio/static folder.

portfolio projects html source files placed in portfolio/template folder.

python source files for apps are in portfolio/apps folder.

To make static files for portfolio project, go to portfolio/static/portfolio/builder and run command: "gulp make-build" in your console.
